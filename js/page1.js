
// 1.
function italicParagraphs(id) {
    var allParagraphs = document.getElementsByClassName(id);
    for (var index = 0; index < allParagraphs.length; index++) {
        allParagraphs[index].style.fontStyle = "italic"
    }
}
italicParagraphs('part1');
//


// 2.
function colorTableRows(id) {
    var table = document.getElementById(id);
    table.rows[0].style.backgroundColor = "green";
    for (var line = 1; line < table.rows.length; line++) {
        var row = table.rows[line];
        row.style.backgroundColor = "yellow";
    }
}
colorTableRows('mytable');
//


// 3.
function makeBootstrapTable(id) {
    var table = document.getElementById(id);
    table.classList.add('table');
}
makeBootstrapTable('mytable');
//


// 4.
function makeButtonTextRed(id) {
    var button = document.getElementById(id);

    if (!button) {
        alert('makeButtonTextRed - no button found');
        return;
    }

    button.style.color = " red ";
}
makeButtonTextRed('btn01');
makeButtonTextRed('btn04');
//


// 5.
function makeButtonSmallRed(id) {
    var button = document.getElementById(id);
    button.style.backgroundColor = "red";
}
makeButtonSmallRed('btn02');
//


// 6.
function removeButton(id) {
    var button = document.getElementById(id);
    button.style.display = "none";
}
removeButton('btn08');
//


// 7.
function changeImagesInPart3() {
    var images = document.getElementsByTagName('img');
    images[0].classList.add('img-circle');
    images[1].classList.add('img-rounded');
    images[2].classList.add('img-thumbnail');
}
changeImagesInPart3()
//


// 8.
function makeLiBorderInUl(id) {
    var list = document.getElementById(id);
    var liTag = list.getElementsByTagName('li');
    for (var index = 0; index < liTag.length; index++)
    {
        liTag[index].style.border = "2px solid red";
    }
}
makeLiBorderInUl('ulist');
//


// 9.
function addLiInOlist(id, max_number) {
    var oList = document.getElementById(id);
    for (var index = 0; index < max_number; index++)
    {
        var liElement = document.createElement('li');
        liElement.innerHTML = "New Fruit " + (index + 1);
        oList.appendChild(liElement);
    }
}
addLiInOlist('olist', 20);